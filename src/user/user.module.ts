import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../entitys/user/user.entity';
import { Log } from '../entitys/log/log.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Log])],
  // exports: [TypeOrmModule],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
