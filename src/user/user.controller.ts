import { Controller, Get, Post, Put, Delete } from '@nestjs/common';
import { UserService } from './user.service';
import { ConfigService } from '@nestjs/config';
import { User } from '../entitys/user/user.entity';

@Controller('user')
export class UserController {
  constructor(
    private userService: UserService,
    private configService: ConfigService,
  ) {}

  // @Get()
  // getUser(): string {
  //   // get a custom configuration value
  //   const db = this.configService.get('db');
  //   console.log(
  //     `@nestjs/config test get db: ${typeof db} ${JSON.stringify(db)}`,
  //   );
  //   return 'get Something';
  // }

  @Get()
  findAll(): object {
    return this.userService.findAll();
  }

  @Get('findById')
  finfindByIdAll(): object {
    return this.userService.findById(4);
  }

  @Get('findUserWithProfile')
  findUserWithProfile(): object {
    return this.userService.findUserWithProfile(5);
  }

  @Get('findUserWithLogs')
  findUserWithLogs(): object {
    return this.userService.findUserWithLogs(5);
  }

  @Get('findLogsRefUser')
  findLogsRefUser(): object {
    return this.userService.findLogsRefUser(5);
  }

  @Post()
  postUser(): any {
    // 新增
    // todo 解析Body参数
    const user = { firstName: 'tom', lastName: 'tip' } as User;
    return this.userService.create(user);
  }

  @Put()
  putUser(): any {
    // 新增
    // todo 解析Body参数
    const user = { firstName: 'to2', lastName: 'tip2' } as User;
    return this.userService.update(1, user);
  }

  @Delete()
  delById(): any {
    // 新增
    // todo 解析Body参数
    return this.userService.removeById(4);
  }
}
