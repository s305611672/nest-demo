import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entitys/user/user.entity';
import { Log } from '../entitys/log/log.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Log)
    private readonly logRepository: Repository<Log>,
  ) {}
  // findAll(): Promise<User[]> {
  //   return new Promise((resolve, reject) => {
  //     resolve([new User()]);
  //   })
  // }
  async create(user: User) {
    const userTmp = await this.userRepository.create(user);
    return this.userRepository.save(userTmp);
  }

  async findAll() {
    return this.userRepository.find();
  }

  async findById(id: number) {
    return this.userRepository.find({ where: { id } });
  }

  async update(id: number, user: Partial<User>) {
    return this.userRepository.update(id, user);
  }

  async removeById(id: number) {
    return this.userRepository.delete(id);
  }

  async findUserWithProfile(id: number) {
    return this.userRepository.find({
      where: { id },
      relations: {
        profile: true,
      },
    });
  }

  async findUserWithLogs(id: number) {
    return this.userRepository.find({
      where: { id },
      relations: {
        logs: true,
      },
    });
  }

  async findLogsRefUser(userId: number) {
    const user = await this.userRepository.find({
      where: { id: userId },
    });
    return this.logRepository.find({
      where: { user },
      // relations: {
      //   user: true,
      // },
    });
  }

  // async remove(id: number): Promise<void> {
  //   await this.usersRepository.delete(id);
  // }

  // postUser(): object {
  //   return {
  //     userName: 'aa',
  //     userId: '123',
  //   };
  // }
}
