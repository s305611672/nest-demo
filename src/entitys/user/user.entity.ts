import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { Log } from '../log/log.entity';
import { Profile } from './profile.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ default: true })
  isActive: boolean;

  // typescript -> 数据库 关联关系 Mapping
  @OneToMany(() => Log, (log) => log.user)
  logs: Log[];

  @OneToOne(() => Profile, (profile) => profile.user)
  profile: Profile;
}
