// export default () => ({
//   port: parseInt(process.env.PORT, 10) || 3000,
//   database: {
//     host: process.env.DATABASE_HOST,
//     port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
//   },
// });

import { readFileSync } from 'fs';
import * as yaml from 'js-yaml';
import { join } from 'path';
import * as merge from 'lodash/merge';
// import * as _ from 'lodash';

// console.log(`cwd: ${process.cwd()}`);
console.log(`NODE_ENV:${process.env.NODE_ENV}`);
const YAML_CONFIG_PATH = join(process.cwd(), 'src', 'config', 'config.yaml');
const common_config = yaml.load(readFileSync(YAML_CONFIG_PATH, 'utf8'));

let merge_config = null;
if (process.env.NODE_ENV === 'development') {
  const YAML_DEV_PATH = join(process.cwd(), 'src', 'config', 'config.dev.yaml');
  const dev_config = yaml.load(readFileSync(YAML_DEV_PATH, 'utf8'));
  merge_config = merge(common_config, dev_config);
} else {
  const YAML_PROD_PATH = join(
    process.cwd(),
    'src',
    'config',
    'config.prod.yaml',
  );
  const prod_config = yaml.load(readFileSync(YAML_PROD_PATH, 'utf8'));
  merge_config = merge(common_config, prod_config);
}
// console.log(`merge_config:${JSON.stringify(merge_config)}`);

export default () => {
  return merge_config;
};
