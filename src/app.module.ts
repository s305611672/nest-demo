import * as Joi from 'joi';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
// import { ConfigEnum } from './enum/config.enum';
import configuration from './config/configuration';
// import * as config from 'config'; // 暂不使用
import { User } from './entitys/user/user.entity';
import { Profile } from './entitys/user/profile.entity';
import { Log } from './entitys/log/log.entity';
// config使用项目/config中的配置文件
// 官方的configuration，我们使用/src/config下的配置文件
// 也可以使用config库进行环境变量的设置，but，这里我们使用官方提供的configuration进行开发
// console.log(`lib config test: ${JSON.stringify(config.get('database'))}`); // 暂不使用

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
      validationSchema: Joi.object({
        NODE_ENV: Joi.string()
          .valid('development', 'production', 'test', 'provision')
          .default('development'),
        // PORT: Joi.number().default(3000),
      }),
    }),
    // TypeOrmModule.forRoot({
    //   type: 'mysql',
    //   host: 'localhost',
    //   port: 3390,
    //   username: 'root',
    //   password: 'sercet@fesfF@bao',
    //   database: '',
    //   entities: [],
    //   synchronize: false,
    // }),
    TypeOrmModule.forRootAsync({
      // name: 'mysqlConnection',
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          type: configService.get('db').mysql.DB_TYPE,
          host: configService.get('db').mysql.DB_HOST,
          port: configService.get('db').mysql.DB_PORT,
          username: configService.get('db').mysql.DB_USERNAME,
          password: configService.get('db').mysql.DB_PASSWORD,
          database: configService.get('db').mysql.DB_DATABASE,
          synchronize: configService.get('db').mysql.DB_SYNC,
          entities: [User, Profile, Log], // "entities": ["dist/**/*.entity{.ts,.js}"],
        } as TypeOrmModuleOptions; // 返回类型设置为TypeOrmModuleOptions
      },
    }),
    UserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
